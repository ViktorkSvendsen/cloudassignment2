package main

import (
	"gopkg.in/mgo.v2"
	"os"
	"log"
	"fmt"
	"time"
	"bytes"
	//"./tests"
	"strings"
	"net/http"
	"io/ioutil"
	"encoding/json"
	"./mgo-2"
	"./github.com/marni/goigc"
)



// toggle for server
var toggleOn bool

func ServerToggle(toggle bool){
	if(!toggle){
		http.ListenAndServe(":8080", nil)
		startTime = time.Now()
		toggle = true
	}
}



// 			starttime when application starts
var startTime = time.Now()

// 			tracks array
var tracks []igc.Track

// 			tracks data


var trackmetadataArray []Trackmetadata

// 			global variables
var hostname, err = os.Hostname()
var uniqueID = fmt.Sprintf("%s:%d", hostname, os.Getpid())


// 			global identifier
func uniqueMonotonicID() string {
	return fmt.Sprintf("%d", time.Since(startTime))
}


// 			global mongoDB
var sessionDatabase *mgo.Session
var collectionDatabase *mgo.Collection



func GetFile(){
	resp, err := http.Get("http::/www.heroku.com/igcinfoimt")
	if err != nil{
		log.Println("getfile Error")
	} else {
		defer resp.Body.Close()
	}
}

// mongoDB
func DatabseConnect() {
	// DB Session
	sessionDatabase, err = mgo.Dial("mongodb://test123:test123@ds145463.mlab.com:45463/h2019873")
	if err != nil {
		log.Println("Failed databse connection: %v\n", err)
	} else {
		//defer sessionDatabase.Close()
		sessionDatabase.SetSafe(&mgo.Safe{})

		log.Println("Connected to DB")
	}
}



func Final(i int64) int64{
	if i < 0{
		return -i
	}else{
		log.Println("Final int", i)
	}
	return i
}

func UpTime()string{
	// calculating time from start of listenAndServe()
	UpTime := time.Now().Unix() - startTime.Unix()
	out := time.Unix(UpTime, 0)
	years, months, days := out.Date()
	hours, minutes, seconds := out.Clock()

	months--
	hours--
	days--

	return fmt.Sprintf("%s%d%s%d%s%d%s%d%s%d%s%d%s" ,"time: ", Final(int64(years - 1970)), "Y", months, "M", days, "D/", hours, "H",minutes, "M", seconds, "S" )

}



// get /api response

func ApiResponse(w http.ResponseWriter) {
	// JSON
	content := map[string]interface{}{
		"ISO 8601": UpTime(),
		"uptime": time.Since(startTime).String(),
		"Info": "Service for IGC tracks.",
		"Version": "v1",
	}
	PrintContent(w, content)
}


// GETReqSent | POSTRequestSent /API/TRACK w
func IGCResponse(w http.ResponseWriter, r *http.Request) {
	// GETReqSent | POSTRequestSent
	if (r.Method == http.MethodGet) {
		// Check for tracks
		if (len(tracks) > 0) {
			var trackIDs []string

			// Fill trackIDs with Track IDs
			for _, trackIndex := range tracks {
				trackIDs = append(trackIDs, trackIndex.GliderID)
			}

			PrintContent(w, trackIDs)
		} else { // No tracks
			NoContent(w)
		}
	} else if (r.Method == http.MethodPost) {
		// Decode and check for required r parameter
		if exists, content := RequestPOSTHandler(r, "url"); exists {
			// Parse IGC
			trackIndex, err := igc.ParseLocation(content["url"].(string))

			if err == nil {
				tracks = append(tracks, trackIndex)

				// Track metadata handling
				trackmetadataArray = append(trackmetadataArray, Trackmetadata{Id: uniqueMonotonicID(), Url: content["url"].(string)})
				UploadTracks(trackmetadataArray[len(trackmetadataArray) - 1])

				// Check WH 
				for i, webhook := range webhooks {
					webhooks[i].Amount += 1;
					if (webhook.Amount == webhook.Trigger) {
						contents := fmt.Sprintf("%d", webhook.Id)
						WebhookSender(webhook.Url, contents)
					}
				}

				// JSON structure
				jsonContent := map[string]interface{}{
					"id": trackIndex.GliderID,
				}

				PrintContent(w, jsonContent)
			} else {
				log.Printf("problem reading the track", err)
				BadRequest(w)
			}
		} else { // Invalid content
			BadRequest(w)
		}
	}
}

// webhooks
var webhooks []Webhook
type Webhook struct {
	Id      string        `bson:"_id,omitempty"`
	Url     string        `json:"url"`
	Trigger int           `json:"trigger"`
	Amount  int           `json:"amount"`
}


// 				get request sent
func IDResponse(w http.ResponseWriter, ID string) {
	// Check and fetch trackIndex with ID
	exists, trackIndex := IDOfTrack(ID)

	if exists {
		// calculate distance
		distance := 0.0
		for i := 0; i < len(trackIndex.Points)-1; i++ {
			distance += trackIndex.Points[i].Distance(trackIndex.Points[i+1])
		}

		// JSON struct
		message := map[string]interface{}{
			"H_date": trackIndex.Date,
			"pilot": trackIndex.Pilot,
			"glider": trackIndex.GliderType,
			"glider_id": trackIndex.GliderID,
			"track_length": distance,
		}

		PrintContent(w, message)
	} else {
		BadRequest(w)
	}
}
type Trackmetadata struct {
	Id      string        `bson:"id,omitempty"`
	Url     string        `json:"url"`
}
// get request /API/IGC/<ID>/<FIELD> w
func ResponseIGCID(w http.ResponseWriter, ID, FIELD string) {
	// Check and fetch trackIndex with ID
	exists, trackIndex := IDOfTrack(ID)

	if exists {
		// Calculate distance
		distance := 0.0
		for i := 0; i < len(trackIndex.Points)-1; i++ {
			distance += trackIndex.Points[i].Distance(trackIndex.Points[i+1])
		}

		switch FIELD {
			case "H_date": PrintContent(w, trackIndex.Date)
			case "pilot": PrintContent(w, trackIndex.Pilot)
			case "glider": PrintContent(w, trackIndex.GliderType)
			case "glider_id": PrintContent(w, trackIndex.GliderID)
			case "track_length": PrintContent(w, distance)
			default: NotFound(w)
		}
	} else {
		NotFound(w)
	}
}

// GETRequestSent /API/TICKER
func ResponseTick(w http.ResponseWriter) {
	tick := time.Now()

	// 			JSON structure
	message := map[string]interface{}{
		"t_latest": trackmetadataArray[len(trackmetadataArray) - 1].Id,
		"t_start": trackmetadataArray[0].Id,
		"t_stop": trackmetadataArray[len(trackmetadataArray) - 1].Id,
		"tracks": TrackIdentifier(),
		"processing": time.Since(tick),
	}

	PrintContent(w, message)
}

//			 get request sent /API/TICKER/LATEST
func LateTickResponse(w http.ResponseWriter) {
	if exists, trackIndex := NewestTrack(); exists {

		PrintContent(w, trackIndex.Date) // Timestamp ? X
		} else { // No tracks in memory
		NotFound(w)
	}
}

// 				get request sent /API/TICKER/TIMESTAMP
func TimestampResponseTick(w http.ResponseWriter, timestamp string) {
	tick := time.Now()

	// JSON structure
	message := map[string]interface{}{
		"t_latest": trackmetadataArray[len(trackmetadataArray) - 1].Id,
		"t_start": trackmetadataArray[0].Id,
		"t_stop": trackmetadataArray[len(trackmetadataArray) - 1].Id,
		"tracks": TrackIdentifier(),
		"processing": time.Since(tick),
	}

	PrintContent(w, message)
}



// 			insert data
func UploadTracks(trackMeta Trackmetadata) {
	DatabseConnect();

	collectionDatabase = sessionDatabase.DB("h2019873").C("tracks")

	err = collectionDatabase.Insert(&trackMeta)
	if err != nil {
		log.Fatal("Problem inserting data: ", err)
	} else {
		log.Println("Uploaded trackIndex meta to databse")
	}

	defer sessionDatabase.Close()
}



func TrackIdentifier() []string {
	// 			check webhooks
	if (len(trackmetadataArray) > 0) {
		var tracksMetaIDs []string

		// fill webhookIDs
		for _, trackMeta := range trackmetadataArray {
			tracksMetaIDs = append(tracksMetaIDs, trackMeta.Id)
		}

		return tracksMetaIDs
	} else { // no tracks
		return nil
	}
}


// post request sent /API/WEBHOOK/NEWTRACK
func WebhookNewTrack(w http.ResponseWriter, r *http.Request) {
	if exists, content := RequestPOSTHandler(r, "webhookURL"); exists {
		// webhook variables from content (muted)
		webhookURL, _ := content["webhookURL"].(string)
		webhookTrigger, _ := content["minTriggerValue"].(int)

		// send Webhook r
		if r := POSTRequestSent(webhookURL, map[string]interface{}{
			"content": "Webhook registered",
			"username": uniqueID,
		}); r != nil {
			// store webhook
			log.Println("HEY MA", uniqueMonotonicID(), uniqueMonotonicID())

			webhooks = append(webhooks, Webhook{uniqueMonotonicID(), webhookURL, webhookTrigger, 0})

			// respond with timestamp of latest trackIndex, new trackIndex IDs and the r time
			PrintContent(w, "LATEST TRACK ID XX");
		} else {
			BadRequest(w)
		}
	} else {
		
	}
}

// 				get request sent
func WebhookIDTrack(w http.ResponseWriter, r *http.Request, ID string) {
	// Convert ID to type int (muted)
	//intID, _ := strconv.Atoi(ID)

	// check the request type
	if (r.Method == http.MethodGet) { // Webhook IDs
		log.Println("GETReqSent WH w. ID")

		// fetch WH w. ID (muted)
		if exists, webhook, _ := WebhookID(ID); exists {
			PrintContent(w, webhook)
		} else {
			NoContent(w)
		}
	} else if (r.Method == http.MethodDelete) {
		log.Println("DELETE WH w. ID")

		if exists, webhook, i := WebhookID(ID); exists {
			webhooks[i] = Webhook{}

			PrintContent(w, webhook)
		} else {
			NoContent(w)
		}

	} else {
		BadRequest(w)
	}
}

func WebhookSender(webhookURL string, message string) {
	if r := POSTRequestSent(webhookURL, map[string]interface{}{
		"content": message,
		"username": uniqueID,
	}); r != nil {
		log.Println("Webhook message delivered", webhookURL, message)
	} else {
		log.Println("Webhook message not delivered", webhookURL, message)
	}
}

// check if webhook with ID exists
func WebhookID(ID string) (bool, Webhook, int) {
	if len(webhooks) != 0 {
		for i, webhook := range webhooks {
			if webhook.Id == ID {
				return true, webhook, i
			}
		}

		return false, Webhook{}, -1
	} else {
		return false, Webhook{}, -1
	}
}

func webhookIdentifiersNew() []string {
		// check webhooks
		if (len(webhooks) > 0) {
			var webhookIDs []string

			// fill webhookIDs
			for _, webhook := range webhooks {
				webhookIDs = append(webhookIDs, webhook.Id)
			}

			return webhookIDs
		} else { // No tracks
			return nil
		}
}

func RequestPOSTHandler(r *http.Request, key string) (bool, map[string]interface{}) {

	body, err := ioutil.ReadAll(r.Body)

	if err == nil {
		log.Println("Received:", string(body))

		// decode
		var payload map[string]interface{}
		json.Unmarshal(body, &payload)

		// check if key exists
		if _, exists := payload[key]; exists {
			return true, payload
		} else { // not found
			log.Println("Request without defined parameter", "«", key, "»", "in payload:", payload)
			return false, nil
		}
	} else { // error
		log.Fatalln(err)
		return false, nil
	}
	return false, nil
}

// 			check if trackId exists
func IDOfTrack(ID string) (bool, igc.Track) {
	if len(tracks) != 0 {
		for _, trackIndex := range tracks {
			if trackIndex.GliderID == ID {
				return true, trackIndex
			}
		}
		return false, igc.NewTrack()
	} else {
		return false, igc.NewTrack()
	}
}

func NewestTrack() (bool, igc.Track) {
	if len(tracks) != 0 {
		return true, tracks[len(tracks) - 1]
	} else {
		return false, igc.NewTrack()
	}
}

// print json
func PrintContent(w http.ResponseWriter, message interface{}) {
	// Encode
	capacity, err := json.Marshal(message)
	if err != nil { log.Fatalln(err) } else {
		// print
	  w.Header().Set("Content-Type", "application/json")
		w.Write([]byte(capacity))
	}
}



// 			No content!	204
func NoContent(w http.ResponseWriter) {
	w.WriteHeader(http.StatusNoContent)
}

// 			Not found! 404
func NotFound(w http.ResponseWriter) {
	w.WriteHeader(http.StatusNotFound)
}

// 			Bad Request! 400
func BadRequest(w http.ResponseWriter) {
	w.WriteHeader(http.StatusBadRequest)
}

// 			Moved Permanentyly! 301
func MovedPermanently(w http.ResponseWriter, r *http.Request, url string) {
	http.Redirect(w, r, url, 301)
}




// get request
func GETReqSent(url string) {
	r, err := http.Get(url)
	if err != nil {
		log.Fatalln(err)
	} else {
		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			log.Fatalln(err)
		} else {
			log.Println(url, r.StatusCode, string(body))
		}
	}
}

// POSTRequestSent r w.
func POSTRequestSent(url string, postContent map[string]interface{}) (*http.Response) {

	capacity, err := json.Marshal(postContent)
	if err != nil {
		log.Fatalln(err)
	}

	// Perform r
	w, err := http.Post(url, "application/json", bytes.NewBuffer(capacity))
	if err != nil {
		log.Fatalln(err)
	}

	// Decode result
	var result map[string]interface{}
	json.NewDecoder(w.Body).Decode(&result)

	log.Println(url, w.StatusCode, result)

	return w
}


// Routes requests
func Routes(w http.ResponseWriter, r *http.Request) {
	log.Println(r.Method, r.URL.Path) // Print r

	parts := strings.FieldsFunc(r.URL.Path, func(c rune) bool { return c == '/' })

	// check endpoint
	if (len(parts) > 0 && parts[0] == "paragliding") {
		// switch between the components of the URL
		switch len(parts) {
			case 1:
				if (parts[0] == "paragliding") {
					MovedPermanently(w, r, "/paragliding/api")
				} else {
					NotFound(w)
				}
			case 2: // API
				if (parts[1] == "api") {
					ApiResponse(w) // Response
				} else if (parts[1] == "flush") { // Flush out data
					tracks = nil
				}
			case 3: // API / TRACK | TICKER
				if (parts[2] == "trackIndex") {
					IGCResponse(w, r) // Response
				} else if (parts[2] == "ticker") {
					ResponseTick(w)
				} else {
					NotFound(w)
				}
			case 4: // API / TRACK / ID | TICKER / LATEST | TIMESTAMP
				if (parts[2] == "ticker" && parts[3] == "latest") { // Latest
					LateTickResponse(w)
				} else if (parts[2] == "ticker") { // Timestamp
					TimestampResponseTick(w, parts[3])
				} else if (parts[2] == "trackIndex") { // ID
					IDResponse(w, parts[3])
				} else if (parts[2] == "webhook" && parts[3] == "new_track") { // Webhook NewTrack
					WebhookNewTrack(w, r)
				} else {
					NotFound(w)
				}
			case 5: // API / TRACK / ID / FIELD
				if (parts[2] == "trackIndex") { // Field
					ResponseIGCID(w, parts[3], parts[4])
				} else if (parts[2] == "webhook" && parts[3] == "new_track") { // Webhook NewTrack WHID
					WebhookIDTrack(w, r, parts[4])
				} else {
					NotFound(w)
				}
			default:
				NotFound(w) // Unreachable
		}
	} else {
		NoContent(w);
	}
}

// 			fetch data
func FetchTrackDatabase() {
	DatabseConnect();

	collectionDatabase = sessionDatabase.DB("h2019873").C("tracks")

	var results []Trackmetadata
	err = collectionDatabase.Find(nil).All(&results)
	if err != nil {
		log.Println("Error finding record: ", err)
	} else {
		log.Println("Fetched data from DB", results)
	}

	defer sessionDatabase.Close()
}

func main() {
	// connect to database
	FetchTrackDatabase()


	http.HandleFunc("/", Routes)
	http.ListenAndServe(":8080", nil)

	ServerToggle(toggleOn)
	GetFile()
	toggleOn = true // just incase
}