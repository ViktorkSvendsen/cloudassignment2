

package test

import (
    "log"
    "time"
    "bytes"
    "net/http"
    "io/ioutil"
    "encoding/json"
)

func Run() {
    // Wait for the serve
    time.AfterFunc(1 * time.Second, All)
}

// Run all tests
func All() {
    //GET("http://localhost:8080/paragliding")

    /*POST("http://localhost:8080/paragliding/api/track", map[string]interface{}{
        "url": "http://skypolaris.org/wp-content/uploads/IGS%20Files/Madrid%20to%20Jerez.igc",
    });
    GET("http://localhost:8080/paragliding/api/track");*/

POST("http://localhost:8080/paragliding/api/webhook/new_track", map[string]interface{}{
"webhookURLl": "test",
})
}

// GET request
func GET(url string) {
	request, err := http.Get(url)
	if err != nil {
		log.Fatalln(err)
	} else {
		body, err := ioutil.ReadAll(request.Body)
		if err != nil {
			log.Fatalln(err)
		} else {
			log.Println(url, request.StatusCode, string(body))
		}
	}
}

// POST request w. message
func POST(url string, message map[string]interface{}) {
	// Parse message
	bytesRepresentation, err := json.Marshal(message)
	if err != nil {
		log.Fatalln(err)
	}

	// Perform request
	response, err := http.Post(url, "application/json", bytes.NewBuffer(bytesRepresentation))
	if err != nil {
		log.Fatalln(err)
	}

	// Decode result
	var result map[string]interface{}
	json.NewDecoder(response.Body).Decode(&result)

	log.Println(url, response.StatusCode, result)
}